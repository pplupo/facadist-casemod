def CPUtemp(leds)
  tasmota.cmd("Sensor29 0," + str(leds[0]))
  tasmota.cmd("Sensor29 1," + str(leds[1]))
  tasmota.cmd("Sensor29 2," + str(leds[2]))
  tasmota.cmd("Sensor29 3," + str(leds[3]))
  tasmota.cmd("Sensor29 4," + str(leds[4]))
  tasmota.cmd("Sensor29 5," + str(leds[5]))
  tasmota.cmd("Sensor29 6," + str(leds[6]))
  tasmota.cmd("Sensor29 7," + str(leds[7]))
  tasmota.cmd("Sensor29 8," + str(leds[8]))
  tasmota.cmd("Sensor29 9," + str(leds[9]))
  tasmota.cmd("Sensor29 10," + str(leds[10]))
  tasmota.cmd("Sensor29 11," + str(leds[11]))
  tasmota.cmd("Sensor29 12," + str(leds[12]))
  tasmota.cmd("Sensor29 13," + str(leds[13]))
  tasmota.cmd("Sensor29 14," + str(leds[14]))
end

def switch1Off()
  tasmota.cmd("Power1 0")
  tasmota.cmd("WebSend [192.168.1.55] /?Switch1=0")
end
tasmota.add_rule("Switch1#State=0", switch1Off)

def switch1On()
  tasmota.cmd("Power1 1")
  tasmota.cmd("WebSend [192.168.1.55] /?Switch1=1")
end
tasmota.add_rule("Switch1#State=1", switch1On)

def switch2Off()
  tasmota.cmd("WebSend [192.168.1.55] /?Switch2=0")
end
tasmota.add_rule("Switch2#State=0", switch2Off)

def switch2On()
  tasmota.cmd("WebSend [192.168.1.55] /?Switch2=1")
end
tasmota.add_rule("Switch2#State=1", switch2On)
package com.pplupo.facadistcasemod.view;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.FacadistCasemod;
import com.pplupo.facadistcasemod.controller.facade.Facade;

public class TrayApplication {
	
	private static final String APP_NAME = "386 Facadist Casemod";
	private static final Logger logger = LoggerFactory.getLogger(TrayApplication.class);
	
	public TrayApplication(FacadistCasemod facadistCasemod) {
		if (SystemTray.isSupported()) {
			SystemTray tray = SystemTray.getSystemTray();
			
			
			Image icon = Toolkit.getDefaultToolkit().getImage(Thread.currentThread().getContextClassLoader().getResource("icon.png"));

			PopupMenu popupMenu = new PopupMenu();

			MenuItem bootRoutine = new MenuItem("Emulate old boot routine");
			bootRoutine.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					showMessage("Emulating old boot routine.");
					Facade.getInstance().emulateBootRoutine();
				}
			});
			popupMenu.add(bootRoutine);
			
			MenuItem openLogFile = new MenuItem("Open log file");
			openLogFile.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						Facade.getInstance().openLogFile();
						showMessage("Opening Log file.");
					} catch (IOException e1) {
						showMessage("Opening Log file: " + e1.getLocalizedMessage());
						logger.error("Error opening log file.",e1);
					}
				}
			});
			popupMenu.add(openLogFile);

			MenuItem exit = new MenuItem("Exit");
			exit.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
			popupMenu.add(exit);

			TrayIcon trayIcon = new TrayIcon(icon, APP_NAME, popupMenu);
			trayIcon.setImageAutoSize(true);

			try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				e.printStackTrace();
				logger.error("Could not initialize the tray icon.", e);
			}
		} else {
			logger.warn("SystemTray is not supported on this platform.");
		}
	}

	private static void showMessage(String message) {
		final TrayIcon trayIcon = SystemTray.getSystemTray().getTrayIcons()[0];
		trayIcon.displayMessage(APP_NAME, message, TrayIcon.MessageType.INFO);
	}

}

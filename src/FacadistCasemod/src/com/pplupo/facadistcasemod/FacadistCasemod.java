package com.pplupo.facadistcasemod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.controller.CPUTemperature;
import com.pplupo.facadistcasemod.controller.WindowsCommands;
import com.pplupo.facadistcasemod.controller.facade.Facade;
import com.pplupo.facadistcasemod.view.TrayApplication;

public class FacadistCasemod {

	private Thread cpuTemperature = new Thread(new CPUTemperature());
	private Facade facade = Facade.getInstance();

	private final Logger logger = LoggerFactory.getLogger(FacadistCasemod.class);

	public void restartCpuTemperatureThread() {
		cpuTemperature.interrupt();
		logger.debug("CPU temperature service stopped.");
		cpuTemperature = new Thread(new CPUTemperature());
		cpuTemperature.start();
		logger.info("CPU temperature service restarted.");
	}
	
	public FacadistCasemod() {
		try {
			new TrayApplication(this);
			
			cpuTemperature.start();
			logger.info("CPU Temperature update service started.");
			
			facade.addSwitchListener((zwitch, status) -> {
				logger.info(zwitch.name() + " action. Status: " + status);
				switch (zwitch) {
				case TURBO:
					switch (status) {
						case ON:
							WindowsCommands.stopScreenSaver();
							break;
						case OFF:
							WindowsCommands.startScreenSaver();
							break;
					}
					break;
				case LOCK:
					switch (status) {
						case ON:
							WindowsCommands.switchToSpeakers();
							break;
						case OFF:
							WindowsCommands.switchToHeadPhones();
							break;
					}
					break;
				}
			});
		} catch (Throwable t) {
			logger.error("Unexpected error. Application terminated.");
		}
	}
	
	public static void main(String[] args) {
			new FacadistCasemod();
	}

}
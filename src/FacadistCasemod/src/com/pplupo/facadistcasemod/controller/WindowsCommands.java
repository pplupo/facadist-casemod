package com.pplupo.facadistcasemod.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WindowsCommands {
	
	private static final Logger logger = LoggerFactory.getLogger(WindowsCommands.class);

	public static void switchToSpeakers() {
		logger.debug("Switching to speakers.");
		execute("SoundVolumeView.exe /SetDefault \"" + Config.lock_off_sound_device_name + "\" all");
		execute("SoundVolumeView.exe /SetVolume \"" + Config.lock_off_sound_device_name + "\"" + Config.lock_off_sound_device_volume);
		execute("SoundVolumeView.exe /SetVolume \"" + Config.lock_off_sound_device_microphone_name + "\" " + Config.lock_off_sound_device_microphone_volume);
	}
	
	public static void switchToHeadPhones() {
		logger.debug("Switching to headphones.");
		execute("SoundVolumeView.exe /SetDefault \"" + Config.lock_on_sound_device_name + "\" all");
		execute("SoundVolumeView.exe /SetVolume \"" + Config.lock_on_sound_device_name + "\"" + Config.lock_on_sound_device_volume);
		execute("SoundVolumeView.exe /SetVolume \"" + Config.lock_on_sound_device_microphone_name + "\" " + Config.lock_on_sound_device_microphone_volume);
	}

	public static void lockWorkstation() {
		logger.debug("Locking workstation.");
		execute("RUNDLL32.exe user32.dll, LockWorkStation");
	}

	public static void startScreenSaver() {
		logger.debug("Starting screensaver.");
//		 execute("powershell.exe -command \"& (Get-ItemProperty �HKCU:Control Panel\\Desktop�).{SCRNSAVE.EXE}\"");
		execute("C:\\Windows\\system32\\scrnsave.scr /s");
	}
	
	public static void stopScreenSaver() {
		logger.debug("Stopping screensaver.");
		execute("taskkill /im scrnsave.scr /f");
//		execute("rundll32 user32.dll,SetCursorPos");
	}
	
	public static void shutdownDOS() {
		logger.debug("Starting DOS.");
		execute("c:\\Program Files\\Oracle\\VirtualBox\\VBoxManage.exe controlvm \"DOS\" poweroff");
	}

	public static void startDOS() {
		logger.debug("Shutting down DOS.");
		execute("c:\\Program Files\\Oracle\\VirtualBox\\VBoxManage.exe startvm \"DOS\"'");
	}
	
	public static void execute(String command) {
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			logger.error("Could not execute command: " + command, e);
		}
	}

}

package com.pplupo.facadistcasemod.controller.facade;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.controller.esp32.ESP32Command;
import com.pplupo.facadistcasemod.controller.esp32.EventListener;

public class Facade implements EventListener {

	private static final Logger logger = LoggerFactory.getLogger(Facade.class);

	private static final Map<Integer, int[]> digitToLed;
	private static final ESP32Command esp32Command = new ESP32Command();
	private final List<SwitchListener> switchListeners;

	private static final Facade instance;

	static {
		instance = new Facade();
		digitToLed = new HashMap<>();
		digitToLed.put(0, new int[] { 1, 1, 1, 1, 1, 1, 0 });
		digitToLed.put(1, new int[] { 0, 1, 1, 0, 0, 0, 0 });
		digitToLed.put(2, new int[] { 1, 1, 0, 1, 1, 0, 1 });
		digitToLed.put(3, new int[] { 1, 1, 1, 1, 0, 0, 1 });
		digitToLed.put(4, new int[] { 0, 1, 1, 0, 0, 1, 1 });
		digitToLed.put(5, new int[] { 1, 0, 1, 1, 0, 1, 1 });
		digitToLed.put(6, new int[] { 1, 0, 1, 1, 1, 1, 1 });
		digitToLed.put(7, new int[] { 1, 1, 1, 0, 0, 0, 0 });
		digitToLed.put(8, new int[] { 1, 1, 1, 1, 1, 1, 1 });
		digitToLed.put(9, new int[] { 1, 1, 1, 1, 0, 1, 1 });
	}

	private Facade() {
		esp32Command.addEventListener(this);
		switchListeners = new ArrayList<>();
	}

	public static enum Status {
		ON("1"), OFF("0");

		private final String espValue;

		Status(String espValue) {
			this.espValue = espValue;
		}

		String getEspValue() {
			return espValue;
		}
	}

	public static enum Led {
		FLOPPY5_25("Power2"), FLOPPY3_5("Power3"), TURBO("Power1");

		private final String espName;

		Led(String espName) {
			this.espName = espName;
		}

		String getEspName() {
			return espName;
		}
	}

	public static enum Switch {
		TURBO("Switch1"), LOCK("Switch2");

		private final String espName;

		Switch(String espName) {
			this.espName = espName;
		}

		String getEspName() {
			return espName;
		}
	}

	public void addSwitchListener(SwitchListener switchListener) {
		switchListeners.add(switchListener);
	}

	public void removeSwitchListener(SwitchListener switchListener) {
		switchListeners.remove(switchListener);
	}

	public void notifySwitchListeners(Switch zwitch, Status status) {
		logger.debug("Switch " + zwitch.name() + " status changed to " + status.name());
		switchListeners.stream().forEach(sl -> sl.actionPerformed(zwitch, status));
	}

	public String updateNumericDisplay(int value) throws IOException {
		logger.debug("Updating numeric display value: " + value);
		int[] leds = new int[15];

		// setting first digit flag if it's greater than 100, removing it from the
		// temperature number
		if (value > 99) {
			leds[0] = 1;
			value = value % 100;
		}
		// translating tens digit to corresponding flag
		System.arraycopy(digitToLed.get(value / 10), 0, leds, 1, 7);
		// translating unit digit to corresponding flags
		System.arraycopy(digitToLed.get(value % 10), 0, leds, 8, 7);

		return esp32Command.send(
				"BR%20CPUtemp(\"" + Arrays.toString(leds).replace(" ", "").replace(",", "").substring(1, 16) + "\")");
	}

	public void emulateBootRoutine() {
		new Thread(() -> {
			logger.debug("Boot routine emulator started.");
			try {
				try (AudioInputStream audioInputStream = AudioSystem
						.getAudioInputStream(Thread.currentThread().getContextClassLoader().getResource("PC.wav"))) {
					try (Clip clip = AudioSystem.getClip()) {
						clip.open(audioInputStream);
						clip.start();
						// Keep the thread alive while the clip is playing
						Thread.sleep(250);
						setStatus(Led.FLOPPY5_25, Status.ON);
						Thread.sleep(844);
						setStatus(Led.FLOPPY5_25, Status.OFF);
						Thread.sleep(300);
						setStatus(Led.FLOPPY3_5, Status.ON);
						Thread.sleep(942);
						setStatus(Led.FLOPPY3_5, Status.OFF);
						Thread.sleep(300);
					}
				}
			} catch (InterruptedException | LineUnavailableException | UnsupportedAudioFileException | IOException e) {
				logger.error("Could not play WAV file.", e);
			}
			logger.debug("Boot routine emulator finished.");
		}).start();
	}

	public String setStatus(Led led, Status status) throws IOException {
		logger.debug("Setting led " + led.name() + " to status " + status.name());
		return esp32Command.send(led.getEspName() + "%20" + status.getEspValue());
	}

	@Override
	public void actionPerformed(String zwitch, String status) {
		notifySwitchListeners(
				Arrays.stream(Switch.values()).filter(e -> e.getEspName().equalsIgnoreCase(zwitch)).findAny()
						.orElse(null),
				Arrays.stream(Status.values()).filter(e -> e.getEspValue().equalsIgnoreCase(status)).findAny()
						.orElse(null));
	}

	public static Facade getInstance() {
		return instance;
	}

	public void openLogFile() throws IOException {
		// Store the log file location
		String logFilePath = null;

		// Iterate through appenders to find the file appender
		Map<String, Appender> appenderMap = ((org.apache.logging.log4j.core.Logger) LogManager.getLogger()).getAppenders();
		
        if (appenderMap == null) {
        	throw new IOException("Log not configured.");
        }
        
        for (Appender appender : appenderMap.values()) {
            if (appender instanceof RollingFileAppender) {
            	logFilePath = ((RollingFileAppender) appender).getFileName();
            	break;
            }
            if (appender instanceof FileAppender) {
            	logFilePath = ((FileAppender) appender).getFileName();
            	break;
            }
        }

        if (logFilePath == null) {
			throw new IOException("Log configuration doesn't use a file appender. File path \"" + logFilePath + "\"");
		}
		// Open the log file with the default text editor
		File logFile = new File(logFilePath);
		if (!logFile.exists()) {
			throw new IOException("Log file not found. File path: \"" + logFilePath + "\"");
		}
		if (!Desktop.isDesktopSupported()) {
			throw new IOException("Desktop not supported. Couldn't open the log file.");
		}
		Desktop.getDesktop().open(logFile);
	}

}

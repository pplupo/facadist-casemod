package com.pplupo.facadistcasemod.controller.facade;

import com.pplupo.facadistcasemod.controller.facade.Facade.Status;
import com.pplupo.facadistcasemod.controller.facade.Facade.Switch;

public interface SwitchListener {
	
	public void actionPerformed(Switch zwitch, Status status);

}

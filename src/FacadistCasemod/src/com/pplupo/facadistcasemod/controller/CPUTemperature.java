package com.pplupo.facadistcasemod.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.controller.facade.Facade;
import com.profesorfalken.jsensors.JSensors;
import com.profesorfalken.jsensors.model.components.Cpu;
import com.profesorfalken.jsensors.model.sensors.Temperature;

public class CPUTemperature implements Runnable {
	
	static final Logger logger = LoggerFactory.getLogger(CPUTemperature.class);

	private int getCPUTemperature() {
		//get the highest core temperature as a fallback in case the package temperature cannot be measured.
		int highestCoreTemperature = 0;
		for (final Cpu cpu : JSensors.get.components().cpus) {
			if (cpu.sensors != null) {
				List<Temperature> temps = cpu.sensors.temperatures;
				for (final Temperature temp : temps) {
					if (highestCoreTemperature < temp.value.intValue()) {
						highestCoreTemperature = temp.value.intValue();
					}
					if (temp.name.contains("Package")) {
						return temp.value.intValue();
					}
				}
			}
		}
		return highestCoreTemperature;
	}

	@Override
	public void run() {
		try {
		updateCPUTemperature();
		} catch (Throwable t) {
			logger.error("CPUTemperature thread exitting.", t);
		}
		logger.error("CPUTemperature thread exitting, no Throwable caught.");
	}

	public void updateCPUTemperature() {
		while (true) {
			try {
				Facade.getInstance().updateNumericDisplay(getCPUTemperature());
			} catch (IOException e) {
				logger.error("Could not send temperature to facade.", e);
			} catch (Throwable t) {
				logger.error("Unexpected error sending temperature to facade.", t);
			}
			Thread.yield();
		}
	}

}

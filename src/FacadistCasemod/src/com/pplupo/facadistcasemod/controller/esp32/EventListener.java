package com.pplupo.facadistcasemod.controller.esp32;

public interface EventListener {
	public void actionPerformed(String zwitch, String status);
}
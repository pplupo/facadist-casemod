package com.pplupo.facadistcasemod.controller.esp32;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.controller.Config;

public class ESP32Command {
	
	private static final Logger logger = LoggerFactory.getLogger(ESP32Command.class);
	
	private SocketListener socketListener;

	public ESP32Command() {
		logger.debug("ESP32 IP set to " + Config.esp32_ip);
		socketListener = new SocketListener();
		socketListener.start();
	}

	public String send(String command) throws IOException {
		String responseReturn = "";
		HttpURLConnection connection = null;
		try {
			try {
				connection = (HttpURLConnection) createRequest(command).openConnection();
				connection.setConnectTimeout(10*1000);
				connection.setRequestMethod("GET");
			} catch (ProtocolException e) {
				logger.debug("Hard-coded 'GET' request method set.", e);
			}
			int responseCode = connection.getResponseCode();
			logger.debug("Response Code: " + responseCode);
			
			// Read the response data
			try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
				String inputLine;
				StringBuilder response = new StringBuilder();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				logger.debug("Response: " + response.toString());
				responseReturn = response.toString();
			}
		} catch (IOException e) {
			logger.error("Error connecting to ESP32.", e);
			throw e;
		} finally {
			if (connection != null)
			try {
				connection.disconnect();
			} catch (Throwable t) {
			}
		}
		return responseReturn;
	}
	
	private URL createRequest(String command) throws MalformedURLException {
		String requestURL = "http://" + Config.esp32_ip + "/cm?cmnd=" + command;
		logger.debug("Request URL: " + requestURL);

		// Create the URL object
		URL url = new URL(requestURL);
		return url;
	}

	public void addEventListener(EventListener eventListener) {
		socketListener.addEventListener(eventListener);
	}

}

package com.pplupo.facadistcasemod.controller.esp32;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pplupo.facadistcasemod.controller.Config;

class SocketListener extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(SocketListener.class);
	
	public Thread socketListener;
	public List<EventListener> eventlisteners = new ArrayList<>();
	
	public void addEventListener(EventListener eventlistener) {
		eventlisteners.add(eventlistener);
	}
	
	public void removeEventListener(EventListener eventlistener) {
		eventlisteners.remove(eventlistener);
	}
	
	public void notifyEventListener(String zwitch, String status) {
		logger.debug("Switch action performed. Switch: " + zwitch + ". Status: " + status);
		eventlisteners.stream().forEach(sl -> sl.actionPerformed(zwitch, status));
	}

	@Override
	public void run() {
		try (ServerSocket serverSocket = new ServerSocket(Config.listening_port)) {
			logger.debug("Server listening for switches on port: " + Config.listening_port);
			while (true) {
				Thread.yield();
				try {
					try (Socket clientSocket = serverSocket.accept()) {
						if (!clientSocket.getInetAddress().getHostAddress().equalsIgnoreCase(Config.esp32_ip)){
							logger.error("Unauthorized: "+ clientSocket.getInetAddress().getHostAddress());
							throw new IOException("Unauthorized: "+ clientSocket.getInetAddress().getHostAddress());
						}
						logger.debug("Connection established with: " + clientSocket.getInetAddress());
						try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {
							String requestLine = in.readLine();
							if (requestLine != null) {
								String queryString = extractQueryString(requestLine);
								logger.debug("Received query string: " + queryString);
								String[] command = queryString.split("=");
								notifyEventListener(command[0], command[1]);
								sendResponse(clientSocket, "200 OK", queryString);
							}
	
						}
					} catch (Exception e) {
						logger.error("Unexpected error listening to request. Retrying.", e);
					}
				} catch (Throwable t) {
					logger.error("Unable to read the request.", t);
				}
			}
		} catch (IOException e) {
			logger.error("Unable to start the service or to read the request.", e);
		} catch (Throwable t) {
			logger.error("SocketListener thread exitting.", t);
		}
		logger.error("SocketListener thread exitting. No Throwable caught.");
	}
	
	private static String extractQueryString(String requestLine) throws IOException {
		String[] parts = requestLine.split(" ");
		if (parts.length >= 2) {
			String[] requestComponents = parts[1].split("\\?");
			if (requestComponents.length >= 2) {
				return requestComponents[1];
			}
		}
		throw new IOException("Could not parse Query String.");
	}

	private static void sendResponse(Socket clientSocket, String status, String responseText) throws IOException {
		try (PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)) {
			out.println("HTTP/1.1 " + status);
			out.println("Content-Type: text/plain");
			out.println("Content-Length: " + responseText.length());
			out.println();
			out.println(responseText);
			out.flush();			
		}
	}
	
}
package com.pplupo.facadistcasemod.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
	
	private static final Logger logger = LoggerFactory.getLogger(Config.class);
	
	private static final Properties properties;
	
	public static final String esp32_ip;
	public static final int listening_port;
	public static final String lock_off_sound_device_microphone_name;
	public static final String lock_off_sound_device_microphone_volume;
	public static final String lock_off_sound_device_name;
	public static final String lock_off_sound_device_volume;
	public static final String lock_on_sound_device_microphone_name;
	public static final String lock_on_sound_device_microphone_volume;
	public static final String lock_on_sound_device_name;
	public static final String lock_on_sound_device_volume;
	public static final String temperature_polling_seconds;
		
	static {
		properties = new Properties();
		try (FileInputStream fileInputStream = new FileInputStream("config.properties")) {
			properties.load(fileInputStream);
		} catch (FileNotFoundException e) {
			logger.error("config.properties not found.");
		} catch (IOException e) {
			logger.error("Could not read config.properties.");
		}
		esp32_ip = properties.getProperty("esp32.ip");
		listening_port = Integer.parseInt(properties.getProperty("listening.port"));
		lock_off_sound_device_microphone_name = properties.getProperty("lock.off.sound.device.microphone.name");
		lock_off_sound_device_microphone_volume = properties.getProperty("lock.off.sound.device.microphone.volume");
		lock_off_sound_device_name = properties.getProperty("lock.off.sound.device.name");
		lock_off_sound_device_volume = properties.getProperty("lock.off.sound.device.volume");
		lock_on_sound_device_microphone_name = properties.getProperty("lock.on.sound.device.microphone.name");
		lock_on_sound_device_microphone_volume = properties.getProperty("lock.on.sound.device.microphone.volume");
		lock_on_sound_device_name = properties.getProperty("lock.on.sound.device.name");
		lock_on_sound_device_volume = properties.getProperty("lock.on.sound.device.volume");
		temperature_polling_seconds = properties.getProperty("temperature.polling.seconds");
	}

}

For more information on what this is for, please visit [https://pages.pplupo.com/2023-08-27-Facadist-casemod/](https://pages.pplupo.com/2023-08-27-Facadist-casemod/).

Administrative privileges are required on Windows (possibly on Linux too), to fetch the CPU temperature. Run Eclipse as Administrator and import as an Eclipse project, otherwise everything should work except that you will get 0 as temperature readings.

I don't have build instructions, I just exported as an executable jar file and created a task on Windows Task Scheduler to run it at user log in. It requires user log in to show the task icon. If you run it on Windows' startup it will run with no GUI.

I want to make it work using serial connection via USB instead of HTTP requests in the future, but I don't know when I'll have the time.
